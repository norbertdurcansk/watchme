
#WatchMe

Description: software WatchMe will work on the request user and only behind other application as guider who manipulate only the voice of "only" used  hand-free. The manipulation is focus to decrease the noise of listening hand-free during you are crossing the traffic street, after will repair noise to same level. For additional the software can redirect outdoor traffic noise into your hearing aids. This will increase the safeness of the crossing the streets.

Focus customers: the runners and walkers and cycle who like to listen music during sporting nevertheless they are close to rush traffic and sometimes they need to cross the streets.

Functions & adjustments:

    adjustment level for decreasing listening music
    adjustment level for redirected outdoor traffic noise
    personal adjustments: with hands-free voice control
    + / - simple add or cancel current traffic crossing place.
    connect to function:
    memory the running or biking or walking road with personal adjustments (of / on)
    adjustment / different manage for ringing mode vs. music
    watching if your smartphone if voice is redirect into hearing aids
    function when you start walk/run/cycle and listening the music through hearing aids 
    - offer you by voice offer: "Can I watch you?" 
    visualization of the road in memory on google map - with the crossing point,
    with function to add new point or cancel used wrongly 
